package com.example.listemployeemanagement.repository;

import java.util.List;
import java.util.Optional;

import com.example.listemployeemanagement.model.Employee;
import com.example.listemployeemanagement.model.Role;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    Optional<Employee> findByEmail (String email);
    boolean existsByEmail(String email);
    List<Role> findByRoles(Long employeeId);
}
