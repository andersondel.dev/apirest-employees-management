package com.example.listemployeemanagement.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.listemployeemanagement.dto.EmployeeEntityRequestDTO;
import com.example.listemployeemanagement.service.JwtService;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

public class JwtAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private static final String HEADER_NAME = "Authorization";
    private final JwtService jwtService;

    public JwtAuthenticationFilter(AuthenticationManager authenticationManager, JwtService jwtService) {
        setAuthenticationManager(authenticationManager);
        this.jwtService = jwtService;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException {
        try {
            EmployeeEntityRequestDTO employeeLoginData = new ObjectMapper().readValue(
                    request.getInputStream(),
                    EmployeeEntityRequestDTO.class);

            return getAuthenticationManager()
                    .authenticate(new UsernamePasswordAuthenticationToken(
                            employeeLoginData.getEmail(),
                            employeeLoginData.getPassword()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
            Authentication authResult) throws IOException, ServletException {
        String generatedToken = jwtService.generateToken(authResult.getName());

        response.addHeader(HEADER_NAME, generatedToken);
        response.getWriter().write("{\"" + HEADER_NAME + "\":" + "\"" + generatedToken + "\"}");
        response.flushBuffer();
    }
}
