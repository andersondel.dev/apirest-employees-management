package com.example.listemployeemanagement.exception;

public class EmailAlreadyExistsException extends Exception {
    
    public EmailAlreadyExistsException(String message) {
        super(message);
    }
    
}