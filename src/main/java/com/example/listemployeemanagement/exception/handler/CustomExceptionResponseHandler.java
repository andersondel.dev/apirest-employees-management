package com.example.listemployeemanagement.exception.handler;

import java.time.LocalDateTime;

import com.example.listemployeemanagement.exception.EmailAlreadyExistsException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtException;

@RestControllerAdvice
public class CustomExceptionResponseHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = { EmailAlreadyExistsException.class })
    public ResponseEntity<Object> handleEmailAlreadyExists(Exception ex, WebRequest request) throws Exception {
        ResponseErrorMessage errorBodyResponse = ResponseErrorMessage.builder()
                                                    .codeError(HttpStatus.BAD_REQUEST.value() + HttpStatus.BAD_REQUEST.getReasonPhrase())
                                                    .message(ex.getMessage())
                                                    .details(request.getDescription(false))
                                                    .time(LocalDateTime.now()).build();

        return new ResponseEntity<>(errorBodyResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = { JwtException.class, ExpiredJwtException.class })
    public ResponseEntity<Object> handleJWTException(Exception ex, WebRequest request) throws Exception {
        ResponseErrorMessage errorBodyResponse = ResponseErrorMessage.builder()
                                                    .codeError(HttpStatus.BAD_REQUEST.value() + HttpStatus.BAD_REQUEST.getReasonPhrase())
                                                    .message("Error during validation of Token! See details of error")
                                                    .details(ex.getLocalizedMessage())
                                                    .time(LocalDateTime.now()).build(); 

        return new ResponseEntity<>(errorBodyResponse, HttpStatus.BAD_REQUEST);                                                
    }
}
