package com.example.listemployeemanagement.service;

import java.util.Optional;

import com.example.listemployeemanagement.dto.EmployeeEntityRequestDTO;
import com.example.listemployeemanagement.exception.EmailAlreadyExistsException;
import com.example.listemployeemanagement.model.Employee;
import com.example.listemployeemanagement.model.Role;
import com.example.listemployeemanagement.repository.EmployeeRepository;
import com.example.listemployeemanagement.repository.RoleRepository;

import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@Service
public class EmployeeService {

    private final EmployeeRepository employeeRepository;
    private final RoleRepository roleRepository;

    public Employee save(EmployeeEntityRequestDTO employeeDTO) throws EmailAlreadyExistsException {
        boolean emailExists = employeeRepository.existsByEmail(employeeDTO.getEmail());

        if(emailExists) {
            throw new EmailAlreadyExistsException("Email already is registered");
        }
        Employee employee = new Employee(employeeDTO.getName(), employeeDTO.getEmail(), employeeDTO.getPassword());
        
        return employeeRepository.save(employee);
    }

    public void addRole(Long employeeId, Long roleId) {
        Optional<Employee> employee = employeeRepository.findById(employeeId);
        Optional<Role> role = roleRepository.findById(employeeId);

        employee.get().getRoles().add(role.get());
        employeeRepository.save(employee.get());
    }

    public void removeRole(Long employeeId, Long roleId) {
        Employee employee = employeeRepository.findById(employeeId).get();

        employee.getRoles().removeIf(roleIt -> roleIt.getId().equals(roleId));
        employeeRepository.save(employee);
    }

    public void changeEnabled(Long employeeId) {
        Optional<Employee> employeeOp = employeeRepository.findById(employeeId);
        boolean enabled = employeeOp.get().isEnabled();

        employeeOp.get().setEnabled(enabled ? false : true);
        employeeRepository.save(employeeOp.get());
    }

}
