package com.example.listemployeemanagement.service;

import java.util.Date;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import com.example.listemployeemanagement.ApplicationContextLoad;
import com.example.listemployeemanagement.model.Employee;
import com.example.listemployeemanagement.repository.EmployeeRepository;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class JwtService {
    
    private static final String HEADER_NAME = "Authorization";

    @Value("${expiration-time}")
    private long durationToken;
    @Value("${s-key}")
    private String sKey;
    @Value("${prefix-token}")
    private String prefixToken;

    public Authentication verifyUserToken(HttpServletRequest request) {

        String token = request.getHeader(HEADER_NAME);
        String email = discompactContentToken(token);

        if(!email.isBlank()) {
            Optional<Employee> employeeFounded = ApplicationContextLoad.getApplicationContext()
                                                        .getBean(EmployeeRepository.class).findByEmail(email);
    
            if(employeeFounded.isPresent()) {
               return new UsernamePasswordAuthenticationToken(employeeFounded.get().getEmail(), 
                                                              employeeFounded.get().getPassword(), 
                                                              employeeFounded.get().getRoles());
            }
        }

        return null;
    }

    public String discompactContentToken(String token) {
        String email;
        try {
            email = Jwts.parser().setSigningKey(sKey)
                            .parseClaimsJws(token.replace(prefixToken + " ", ""))
                            .getBody().getSubject();
        } catch (JwtException | IllegalArgumentException e) {
            throw e;
        }
       
        return email;
    }
    
    public boolean isExpired(String token) {
        boolean expired = false;
        
        try {
            Jwts.parser()
                .setSigningKey(sKey)
                .parseClaimsJws(token.replace(prefixToken + " ", ""));
        } catch (ExpiredJwtException e) {
            expired = true;
        }
        
        return expired;
    }

    public String generateToken(String email) {
        String token = Jwts.builder().setSubject(email)
                      .setIssuedAt(new Date(System.currentTimeMillis()))
                      .setExpiration(new Date(System.currentTimeMillis() + durationToken))
                      .signWith(SignatureAlgorithm.HS256, sKey)
                      .compact();
        return prefixToken + " " + token;
    }

}
