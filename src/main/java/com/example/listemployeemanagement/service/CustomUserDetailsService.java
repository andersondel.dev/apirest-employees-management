package com.example.listemployeemanagement.service;

import java.util.ArrayList;

import com.example.listemployeemanagement.model.Employee;
import com.example.listemployeemanagement.repository.EmployeeRepository;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@Service
public class CustomUserDetailsService implements UserDetailsService {

    private final EmployeeRepository employeeRepository;
 
    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
         Employee employeeFounded = employeeRepository.findByEmail(email)
                                    .orElseThrow(() -> new UsernameNotFoundException("Not found user with email: " + email));

         return new User(employeeFounded.getEmail(), employeeFounded.getPassword(), employeeFounded.getRoles());
    }
}
