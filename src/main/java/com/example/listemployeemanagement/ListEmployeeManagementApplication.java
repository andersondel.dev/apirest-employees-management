package com.example.listemployeemanagement;

import java.util.Arrays;
import java.util.List;

import com.example.listemployeemanagement.model.Employee;
import com.example.listemployeemanagement.model.Role;
import com.example.listemployeemanagement.repository.EmployeeRepository;

import org.modelmapper.ModelMapper;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@SpringBootApplication
public class ListEmployeeManagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(ListEmployeeManagementApplication.class, args);
	}

	private final EmployeeRepository employeeRepository;

	@Bean
	CommandLineRunner runner() {
		return initialData -> {

			Role normal = new Role("ROLE_NORMAL");
			Role admin = new Role("ROLE_ADMIN");
			Role intern = new Role("ROLE_INTERN");
			
			List<Employee> employees = Arrays.asList(
				new Employee("Cristiano Ronaldo", "email@email.com","password1").addRole(admin).addRole(normal),
				new Employee("Admin", "email1@email.com","password2").addRole(admin),
				new Employee("User", "email2@email.com","password3").addRole(normal),
				new Employee("Neymar Jr.", "email3@email.com","password4").addRole(intern),
				new Employee("Messi TuMadre", "email4@email.com","password5").addRole(admin).addRole(normal)
			);

			employeeRepository.saveAll(employees);
		};
	}

	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}

}
