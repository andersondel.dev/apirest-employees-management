package com.example.listemployeemanagement.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import com.example.listemployeemanagement.dto.EmployeeEntityRequestDTO;
import com.example.listemployeemanagement.exception.EmailAlreadyExistsException;
import com.example.listemployeemanagement.model.Employee;
import com.example.listemployeemanagement.model.Role;
import com.example.listemployeemanagement.repository.EmployeeRepository;
import com.example.listemployeemanagement.service.EmployeeService;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
@RequestMapping("/employees")
public class EmployeeController {

    private final EmployeeRepository employeeRepository;
    private final EmployeeService employeeService;

    @GetMapping
    public ResponseEntity<List<Employee>> getAll() {
        List<Employee> allEmployees = employeeRepository.findAll();

        if (allEmployees.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(allEmployees);
    }

    @GetMapping("/{employeeId}")
    public ResponseEntity<Employee> getOne(@PathVariable Long employeeId) {
        Optional<Employee> employeeFounded = employeeRepository.findById(employeeId);

        if(employeeFounded.isPresent()) {
            return ResponseEntity.ok(employeeFounded.get());
        }
        return ResponseEntity.notFound().build();
    }

    @PostMapping
    public ResponseEntity<Employee> create(@RequestBody @Valid EmployeeEntityRequestDTO employeeDTO) throws EmailAlreadyExistsException {
        Employee employeeSaved = employeeService.save(employeeDTO);
        return new ResponseEntity<Employee>(employeeSaved, HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<Employee> updateLoggedUser(@RequestBody Employee employeeEdit) {
        Employee employeeLogged = this.getEmployeeLogged();

        employeeLogged = employeeRepository.findByEmail(employeeLogged.getEmail()).get();
        employeeEdit.setId(employeeLogged.getId());

        employeeRepository.save(employeeEdit);
        return ResponseEntity.ok().build();
    }

    private Employee getEmployeeLogged() {
        Authentication userLogged = SecurityContextHolder.getContext().getAuthentication();

        Employee employee = new Employee(null, userLogged.getName(), (String) userLogged.getCredentials());
        userLogged.getAuthorities().forEach(aut -> {
            employee.addRole((Role) aut);
        });
        return employee;
    }
    
}
