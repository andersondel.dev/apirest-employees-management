package com.example.listemployeemanagement.controller;

import java.util.List;

import com.example.listemployeemanagement.model.Employee;
import com.example.listemployeemanagement.model.Role;
import com.example.listemployeemanagement.repository.EmployeeRepository;
import com.example.listemployeemanagement.repository.RoleRepository;
import com.example.listemployeemanagement.service.EmployeeService;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@RestController
@RequestMapping(value = "/admin/employees")
public class AdminController {

    private final EmployeeRepository employeeRepository;
    private final RoleRepository roleRepository;
    private final EmployeeService employeeService;

    @GetMapping("/{employeeId}")
    public ResponseEntity<List<Role>> getAllRoles() {
        List<Role> roles = roleRepository.findAll();

        if(roles.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(roles);
    }

    @PutMapping("/{employeeId}")
    public ResponseEntity<Employee> changeEnabled(@PathVariable Long employeeId) {
        if(employeeRepository.existsById(employeeId)) {
            employeeService.changeEnabled(employeeId);
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.notFound().build();
    }

    @PatchMapping("/{employeeId}/{roleId}")
    public ResponseEntity<Employee> addRole(@PathVariable Long employeeId, @PathVariable Long roleId) {

        if(employeeRepository.existsById(employeeId) && roleRepository.existsById(roleId)) {
            employeeService.addRole(employeeId, roleId);
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.notFound().build();
    }

    @DeleteMapping("/{employeeId}/{roleId}")
    public ResponseEntity<Employee> removeRole(@PathVariable Long employeeId, @PathVariable Long roleId) {
        if(employeeRepository.existsById(employeeId) && roleRepository.existsById(roleId)) {
            employeeService.removeRole(employeeId, roleId);
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.notFound().build();
    }

}
