package com.example.listemployeemanagement.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.example.listemployeemanagement.repository.EmployeeRepository;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@TestPropertySource(properties = { "s-key=cGFpZGVmYW1pbGlh", "expiration-time=1800000" }) 
public class JwtServiceTest {

    private JwtService jwtService;

    @Value("${expiration-time}")
    private long durationToken;
    @Value("${s-key}")
    private String sKey;
    @Value("${prefix-token}")
    private String prefixToken;

    @MockBean
    private EmployeeRepository employeeRepository;

    @BeforeEach
    public void setUp() {
        jwtService = new JwtService();
        ReflectionTestUtils.setField(jwtService, "sKey", "cGFpZGVmYW1pbGlh");
        ReflectionTestUtils.setField(jwtService, "prefixToken", "Bearer");
        ReflectionTestUtils.setField(jwtService, "durationToken", 1800000);
    }

    @Test
    @DisplayName("Should return a token jwt for autentication")
    public void shouldGenerateToken() {
        String email = "adn@gmail.com";

        String generatedToken = jwtService.generateToken(email);

        System.err.println(generatedToken);

        Assertions.assertThat(generatedToken).startsWith("Bearer")
                                             .contains(".");
    }

    @Test
    @DisplayName("Should return correct email used for generated token jwt")
    public void shouldDiscompactToken() {
        String email_espected = "adn@gmail.com";
        String token_header = getTokenTest();

        String email_token = jwtService.discompactContentToken(token_header);
        assertEquals(email_espected, email_token);
    }

    @Test
    @DisplayName("Should verify token expired")
    public void shouldVerifyTokenIsExpired() {
        String token_header = getTokenTest();

        boolean expired = jwtService.isExpired(token_header);

        assertEquals(true, expired);
    }

    private String getTokenTest() {
        String token_header = "Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJlbWFpbEBlbWFpbC5jb20iLCJpYXQiOj"
                               + "E2NDMzMTE5NDUsImV4cCI6MTY0MzMxMjI0NX0.H0jqkeiXh1J8y7JqVwlKPZ4Fs_o1EEZFrKGMs45VgWA";
        return token_header;
    }

    
    
    
}
